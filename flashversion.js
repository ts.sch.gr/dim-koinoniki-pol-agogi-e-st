function getFlashPluginVersion() 
{ 
  var version = { 
        major: -1, 
        minor: -1, 
        installed: false,
        scriptable: false
      };

  var plugin = navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin; 
  if (!plugin) 
  {
    return version;
  }

  version.installed = true;

  var description = plugin.description; 
  
  // use RegExp to obtain the relevant version strings 
  // obtain an array of size 2 with version information
  
  var versionArray = description.match(/[\d.]+/g); 
  
  if (!versionArray)
  {
    return version;
  }

  if (versionArray.length >= 1 && !isNaN(versionArray[0]))
  {
    version.major = parseFloat(versionArray[0]);
  }

  if (versionArray.length >= 2 && !isNaN(versionArray[1]))
  {
    version.minor = parseFloat(versionArray[1]);
  }
    
  if (version.major < 6 || navigator.product != 'Gecko')
  {
    return version;
  }

  if (version.major > 6 || version.minor >= 47)
  {
    version.scriptable = true;
  }

  return version;
}

function identifyFlash() 
{ 
  if ((window.ActiveXObject) && 
      (navigator.userAgent.indexOf("MSIE")!= -1) && (navigator.userAgent.indexOf("Windows") != -1))
  {
    document.write("<p>This browser is an <b>IE browser<\/b> which" +
        " supports ActiveX -- this article is about scripting the plugin in Netscape Gecko browsers.<\/p>");
    return;
  }
  var flashversion = getFlashPluginVersion();

  if (!flashversion.installed)
  {
    document.write("<p>Flash is not installed as a plugin. " +
                   "You need to install the latest version available from " +
                   "<a href='http://www.macromedia.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&P5_Language=English'>here.<\/a>" +
                   "<\/p>");
    return;
  }

  if (flashversion.major == -1 || flashversion.minor == -1)
  {
    document.write("<p>Flash is installed as a plugin but the version could not be determined.<\/p>");
    return;
  }

  if (flashversion.scriptable)
  {
    document.write("<p>Flash is installed as a plugin and is scriptable in Gecko.<\/p>");
    return;
  }
  if (!flashversion.scriptable)
  {
	document.write("<p>Flash is not scriptable and so the examples in this article will not work ." +
                   "You need to install the latest scriptable version available from " +
                   "<a href='http://www.macromedia.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&P5_Language=English'>here.<\/a>" +
                   "<\/p>");
    return;
  }

  document.write("<p>Flash is installed as a plugin and " +
                 "is version " + flashversion.major + "r" +
                 flashversion.minor + "<\/p>");
} 
